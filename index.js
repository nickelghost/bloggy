const Koa = require('koa');

const app = new Koa();

const koaStatic = require('koa-static');

app.use(koaStatic('./public'));

const router = require('./app/router');
const sequelize = require('./config/sequelize');

app.use((ctx, next) => {
  ctx.sequelize = sequelize;
  next();
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);
