const Post = require('../models/post');

const PostController = {
  async create({ response, sequelize }) {
    Post(sequelize).sync();
    const post = await Post(sequelize).create({
      title: 'Post Title',
      content: 'Post Content',
    });
    response.body = post.toJSON();
    console.log(post.toJSON());
  },
};

module.exports = PostController;
