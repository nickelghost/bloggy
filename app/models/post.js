const Sequelize = require('sequelize');

const Post = (sequelize) => {
  const model = sequelize.define('post', {
    title: Sequelize.STRING,
    content: Sequelize.TEXT,
  });

  return model;
};

module.exports = Post;
