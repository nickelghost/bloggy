const Router = require('koa-router');

const router = new Router();

const PostController = require('./controllers/PostController');

router.post('/api/post/create', PostController.create);

module.exports = router;
